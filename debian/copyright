Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: JFreeChart
Source: https://github.com/jfree/jfreechart

Files: *
Copyright: David Gilbert (david.gilbert@object-refinery.com) and others
           2000-2013, Object Refinery Limited and Contributors
License: LGPL-2.1+
Comment:
 Upstream Authors:
 .
 Thanks to the following developers who have contributed code to this
 class library:  Anthony Boulestreau, Jeremy Bowman, J. David
 Eisenberg, Paul English, Hans-Jurgen Greiner, Bill Kelemen, Achilleus
 Mantzios, Thomas Meier, Krzysztof Paz, Andrzej Porebski, Nabuo
 Tamemasa, Mark Watson and Hari.

Files: debian/*
Copyright: 2003,      Christian Bayle <bayle@debian.org>
           2005,      Arnaud Vandyck <avdyk@debian.org>
           2006,      Wolfgang Baer <WBaer@gmx.de>
           2006,      Matthias Klose <doko@debian.org>
           2007,      Michaek Koch <konqueror@gmx.de>
           2007,      Kumar Appaiah <akumar@ee.iitm.ac.in>
           2008,      Varun Hiremath <varun@debian.org>
           2009,      Vincent Fourmond <fourmond@debian.org>
           2010-2011, Damien Raude-Morvan <drazzib@debian.org>
           2012,      Jakub Adam <jakub.adam@ktknet.cz>
           2014-2016, Emmanuel Bourg <ebourg@apache.org>
           2015-2016, Markus Koschany <apo@debian.org>
License: LGPL-2.1+

License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this package; if not, write to the Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.
